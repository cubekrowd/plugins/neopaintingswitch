package plugin.arcwolf.neopaintingswitch;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.flags.Flags;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class WorldGuardIntegration {
    // Put this in a separate class, so NoClassDefFoundErrors don't occur when
    // WorldGuard isn't loaded.
    public static boolean canModifyPainting(Player player, Entity painting) {
        var loc = painting.getLocation();
        var vec = BlockVector3.at(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
        var wrapped = WorldGuardPlugin.inst().wrapPlayer(player);
        return WorldGuard.getInstance().getPlatform().getRegionContainer()
                .get(BukkitAdapter.adapt(loc.getWorld())).getApplicableRegions(vec)
                .testState(wrapped, Flags.BUILD);
    }
}
