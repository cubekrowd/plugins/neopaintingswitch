package plugin.arcwolf.neopaintingswitch;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import org.bukkit.Server;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class neoPaintingSwitch extends JavaPlugin {
    public WorldGuardPlugin wgp;
    public boolean worldguard = false;

    @Override
    public void onEnable() {
        Server server = this.getServer();
        PluginManager pm = server.getPluginManager();
        wgp = getWorldGuard();
        worldguard = wgp != null;
        pm.registerEvents(new npPlayerEvent(this), this);
        pm.registerEvents(new npPaintingBreakEvent(), this);
    }

    // get worldguard plugin
    private WorldGuardPlugin getWorldGuard() {
        Plugin plugin = getServer().getPluginManager().getPlugin("WorldGuard");

        // WorldGuard may not be loaded
        if (plugin == null || !(plugin instanceof WorldGuardPlugin)) { return null; }

        return (WorldGuardPlugin) plugin;
    }
}
