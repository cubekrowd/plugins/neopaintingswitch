# neoPaintingSwitch

This plugin uses the same basic idea as located [here](http://forums.bukkit.org/threads/inactive-edit-paintingswitch-v0-1-choose-paintings-with-ease-672.5788/) and [here](http://forums.bukkit.org/threads/edit-paintingswitch-v0-4-scroll-through-paintings-1185.24604/). Basically, it simplifies the selecting of paintings and saves a lot of time.

This plugin was originally created by [Edward Hand](http://forums.bukkit.org/members/edward-hand.13332/) (credit to him!) and also eventually updated by [MadMonkeyCo](http://forums.bukkit.org/members/madmonkeyco.22820/). However it was abandoned again and picked up by [Arcwolf](https://bukkit.org/members/arcwolf.6518/). After some time Traks decided to modernise it for CubeKrowd.

This plugin has the following features:

* Allows you to scroll through paintings by aiming and right clicking at them and using the mouse wheel to scroll through paintings.
* The plugin will remember what painting was last used and will attempt to use that painting again for future placed paintings.
* Requires the permission "neopaintingswitch.use".
* Supports WorldGuard.
